# syntax=docker/dockerfile:1

FROM devopworks/golang-upx:1.18.1-1 as builder
WORKDIR /src
##COPY minio /go/bin/minio
RUN GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0 \
    go build -o /go/bin/minio && \
    strip /go/bin/minio &&
    
FROM alpine:3.16.2

ENV MINIO_ACCESS_KEY_FILE=access_key \
    MINIO_SECRET_KEY_FILE=secret_key \
    MINIO_ROOT_USER_FILE=access_key \
    MINIO_ROOT_PASSWORD_FILE=secret_key \
    MINIO_KMS_SECRET_KEY_FILE=kms_master_key \
    MINIO_UPDATE_MINISIGN_PUBKEY="RWTx5Zr1tiHQLwG9keckT0c45M3AGeHD6IvimQHpyRywVWGbP1aVSGav" \
    MINIO_CONFIG_ENV_FILE=config.env \
    PATH=/opt/bin:$PATH

USER root

RUN mkdir -p /usr/bin/
RUN mkdir -p /data
RUN mkdir -p /opt/bin

COPY --from=builder /go/bin/minio /opt/bin/minio

#COPY dockerscripts/verify-minio.sh /usr/bin/verify-minio.sh
COPY dockerscripts/docker-entrypoint.sh /usr/bin/docker-entrypoint.sh
COPY CREDITS /licenses/CREDITS
COPY LICENSE /licenses/LICENSE



RUN  chmod +x /opt/bin/minio && \
chmod 777 /data && \
     chmod +x /usr/bin/docker-entrypoint.sh 
     ##&& \
     ##chmod +x /usr/bin/verify-minio.sh 
     ##&& \
     ##/usr/bin/verify-minio.sh

EXPOSE 9000 9001
##USER 1001
ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]

VOLUME ["/data"]

CMD ["minio"]
